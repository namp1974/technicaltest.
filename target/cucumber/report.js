$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri('cucumberJava4\cucumberJava.feature');
formatter.feature({
  "line": 1,
  "name": "Como cliente quiero seleccionar para origen y destino para viajar",
  "description": "",
  "id": "como-cliente-quiero-seleccionar-para-origen-y-destino-para-viajar",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 3,
  "name": "Select departure and destination city doesn\u0027t exist",
  "description": "",
  "id": "como-cliente-quiero-seleccionar-para-origen-y-destino-para-viajar;select-departure-and-destination-city-doesn\u0027t-exist",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I have open the browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I open blazedemo website",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Choose departure city option should not exits",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Choose destination city option should not exits",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Click button",
  "keyword": "Then "
});
formatter.match({
  "location": "annotation.openBrowser()"
});
formatter.result({
  "duration": 8394848595,
  "status": "passed"
});
formatter.match({
  "location": "annotation.goToBlazedemo()"
});
formatter.result({
  "duration": 4573473525,
  "status": "passed"
});
formatter.match({
  "location": "annotation.selectCity()"
});
formatter.result({
  "duration": 88957478,
  "status": "passed"
});
formatter.match({
  "location": "annotation.selectCity2()"
});
formatter.result({
  "duration": 42909885,
  "status": "passed"
});
formatter.match({
  "location": "annotation.selectCity3()"
});
formatter.result({
  "duration": 1242679144,
  "status": "passed"
});
});