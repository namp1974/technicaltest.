package cucumberjava2; 

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.firefox.FirefoxDriver; 

import cucumber.annotation.en.Given; 
import cucumber.annotation.en.Then; 
import cucumber.annotation.en.When; 

public class annotation { 
   WebDriver driver = null; 
	
   @Given("^I have open the browser$") 
   public void openBrowser() { 
	  System.setProperty("webdriver.gecko.driver","C:\\geckodriver-v0.23.0-win64\\geckodriver.exe");
      driver = new FirefoxDriver(); 
   } 
	
   @When("^I open blazedemo website$") 
   public void goToBlazedemo() { 
      driver.navigate().to("http://www.blazedemo.com/login"); 
   } 

   @Then("^Fill email value$") 
   public void email() { 
      if(driver.findElement(By.name("email")).isEnabled()) { 
    	  driver.findElement(By.name("email")).sendKeys("hola@fake.cl");
         System.out.println("Test 1 Pass"); 
      } else { 
         System.out.println("Test 1 Fail"); 
      } 
   } 
   
   @Then("^Fill password value$") 
   public void pass() { 
      if(driver.findElement(By.name("password")).isEnabled()) { 
    	  driver.findElement(By.name("password")).sendKeys("hola");
         System.out.println("Test 1 Pass"); 
      } else { 
         System.out.println("Test 1 Fail"); 
      } 
   }
   
   @Then("^Click button$") 
   public void selectCity3() { 
	   if(driver.findElement(By.className("btn-primary")).isEnabled()){
		  driver.findElement(By.className("btn-primary")).click();
	        System.out.println("Test 1 Pass"); 
	     } else { 
	         System.out.println("Test 1 Fail"); 
	      }   }
   } 
   
   
